#!/usr/bin/env python3

# https://gitlab.com/s3lph/prometheus-collector-dirvish
#
# Copyright 2020 s3lph
#
# Permission is hereby granted, free of charge, to any person
# obtaining a copy of this software and associated documentation files
# (the "Software"), to deal in the Software without restriction,
# including without limitation the rights to use, copy, modify, merge,
# publish, distribute, sublicense, and/or sell copies of the Software,
# and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
# BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
# ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#
# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2020 s3lph


import os
import sys
import argparse
from datetime import datetime


PARSE_FAIL = '<PARSE_FAIL>'


def parse_dirvish_conf(path = '/etc/dirvish/master.conf'):
    conf = {}
    with open(path, 'r') as f:
        lines = f.readlines()
    lk = None
    for line in lines:
        if len(line.strip()) == 0 or line.strip().startswith('#'):
            continue
        if line.startswith(' ') or line.startswith('\t') and lk:
            conf[lk].append(line.strip())
            continue
        if ':' not in line:
            continue
        k, v = line.split(':', 1)
        lk = k.strip().lower()
        if len(v.strip()) == 0:
            conf[lk] = []
            continue
        else:
            conf[lk] = v.strip()
    return conf


def traverse_bank(path):
    vaults = []
    stack = [path]
    while len(stack) > 0:
        path = stack.pop()
        with os.scandir(path) as entries:
            if len([1 for e in entries if e.name == 'dirvish' and e.is_dir()]) > 0:
                vaults.append(path)
                continue
        with os.scandir(path) as entries:
            for entry in entries:
                if entry.is_dir():
                    stack.append(entry.path)
    return vaults


def find_vaults(bank):
    vaults = []
    vaults.extend(traverse_bank(bank))
    return vaults


def parse_dirvish_image(path: str, snapshot_check_file: str):
    try:
        summary = os.path.join(path, 'summary')
        status = parse_dirvish_conf(summary)
        image = {
            'success': 1 if status['status'] == 'success' else 0,
            'begin': datetime.strptime(status['backup-begin'], '%Y-%m-%d %H:%M:%S'),
            'end': datetime.strptime(status['backup-complete'], '%Y-%m-%d %H:%M:%S'),
            'vault': status['vault'],
            'bank': status['bank'],
            'branch': status['branch']
        }
        if snapshot_check_file is not None:
            fname = os.path.join(path, 'tree', snapshot_check_file)
            if os.path.exists(fname):
                stat = os.lstat(fname)
                image['snapshot'] = datetime.fromtimestamp(stat.st_mtime)
        return image
    except:
        return PARSE_FAIL


def parse_dirvish_histfile(vault, path, snapshot_check_file: str):
    images = {}
    branches = {}
    with open(path, 'r') as f:
        lines = f.readlines()
    for line in lines:
        try:
            if line.startswith('#'):
                continue
            image_name, date, ref, expiry = line.split('\t', 3)
            image = parse_dirvish_image(os.path.join(vault, image_name), snapshot_check_file)
            if image is PARSE_FAIL:
                continue
            branch = image['branch']
            if branch not in branches:
                branches[branch] = [image]
            else:
                branches[branch].append(image)
            images[image_name] = image
        except:
            return PARSE_FAIL
    return branches


def parse_vault_history(vault, snapshot_check_file: str):
    branches = {}
    locked = False
    with os.scandir(os.path.join(vault, 'dirvish')) as entries:
        for entry in entries:
            if entry.is_file() and entry.name.endswith('.hist'):
                branches.update(parse_dirvish_histfile(vault, entry.path, snapshot_check_file))
            elif entry.is_file() and entry.name == 'lock_file':
                locked = True
    return branches, locked


def parse_all_banks(master_conf: str, snapshot_check_file: str):
    try:
        conf = parse_dirvish_conf(master_conf)
        banks = {}
        for bank in conf['bank']:
            try:
                vault_paths = find_vaults(bank)
                vaults = {}
                for vault in vault_paths:
                    try:
                        vaults[vault] = parse_vault_history(vault, snapshot_check_file)
                    except:
                        vaults[vault] = PARSE_FAIL
                banks[bank] = vaults
            except:
                banks[bank] = PARSE_FAIL
    except:
        banks = PARSE_FAIL
    return banks


def generate_prometheus_response(banks):
    print('# TYPE dirvish_banks gauge')
    print('# HELP dirvish_banks Number of banks, -1 on failure')
    if banks is PARSE_FAIL:
        print(f'dirvish_banks -1')
        return
    print(f'dirvish_banks {len(banks)}')
    
    print('# TYPE dirvish_vaults gauge')
    print('# HELP dirvish_vaults Number of vaults in a bank, -1 on failure')
    for bank, vaults in banks.items():
        if vaults is PARSE_FAIL:
            print(f'dirvish_vaults{{bank="{bank}"}} -1')
            continue
        print(f'dirvish_vaults{{bank="{bank}"}} {len(vaults)}')
    
    print('# TYPE dirvish_branches gauge')
    print('# HELP dirvish_branches Number of branches in a vault, -1 on failure')
    print('# TYPE dirvish_vault_locked gauge')
    print('# HELP dirvish_vault_locked 1 if there exists a lock file in the vault, 0 otherwise')
    for bank, vaults in banks.items():
        if vaults is PARSE_FAIL:
            continue
        for vault, _branches in vaults.items():
            if _branches is PARSE_FAIL:
                print(f'dirvish_branches{{bank="{bank}",vault="{vault}"}} -1')
                continue
            branches, locked = _branches
            print(f'dirvish_branches{{bank="{bank}",vault="{vault}"}} {len(branches)}')
            print(f'dirvish_vault_locked{{bank="{bank}",vault="{vault}"}} {1 if locked else 0}')
    
    print('# TYPE dirvish_images gauge')
    print('# HELP dirvish_images Number of images in a branch, -1 on failure')
    for bank, vaults in banks.items():
        if vaults is PARSE_FAIL:
            continue
        for vault, _branches in vaults.items():
            if _branches is PARSE_FAIL:
                continue
            branches, locked = _branches
            for branch, images in branches.items():
                if images is PARSE_FAIL:
                    print(f'dirvish_images{{bank="{bank}",vault="{vault}",branch="{branch}"}} -1')
                    continue
                print(f'dirvish_images{{bank="{bank}",vault="{vault}",branch="{branch}"}} {len(images)}')
    
    print('# TYPE dirvish_latest_backup_success gauge')
    print('# HELP dirvish_latest_backup_success 1 if the latest backup was successful, 0 otherwise')
    print('# TYPE dirvish_latest_backup_begin gauge')
    print('# HELP dirvish_latest_backup_begin UNIX epoch timestamp when the backup began, in milliseconds')
    print('# TYPE dirvish_latest_backup_end gauge')
    print('# HELP dirvish_latest_backup_end UNIX epoch timestamp when the backup finished, in milliseconds')
    print('# TYPE dirvish_latest_backup_duration gauge')
    print('# HELP dirvish_latest_backup_duration Duration of the backup, in milliseconds')
    print('# TYPE dirvish_latest_backup_branch_length gauge')
    print('# HELP dirvish_latest_backup_branch_length Number of backup images in the branch')
    print('# TYPE dirvish_latest_backup_snapshot_mtime gauge')
    print('# HELP dirvish_latest_backup_snapshot_mtime UNIX epoch timestamp when the pre-client snaphot finished, if configured, in milliseconds')
    for bank, vaults in banks.items():
        if vaults is PARSE_FAIL:
            continue
        for vault, _branches in vaults.items():
            if _branches is PARSE_FAIL:
                continue
            branches, locked = _branches
            for branch, images in branches.items():
                if images is PARSE_FAIL:
                    continue
                try:
                    latest = images[-1]
                    success = latest['success']
                    begin = int(latest['begin'].timestamp() * 1000)
                    end = int(latest['end'].timestamp() * 1000)
                    duration = int((latest['end'] - latest['begin']).total_seconds())
                    rbank = latest['bank']
                    rvault = latest['vault']
                    rbranch = latest['branch']
                    if 'snapshot' in latest:
                        snapshot = int(latest['snapshot'].timestamp() * 1000)
                except:
                    latest = PARSE_FAIL
                if latest is PARSE_FAIL:
                    print(f'dirvish_latest_backup_success{{bank="{bank}",vault="{vault}",branch="{branch}"}} -1')
                    continue
                print(f'dirvish_latest_backup_success{{bank="{rbank}",vault="{rvault}",branch="{rbranch}"}} {success}')
                print(f'dirvish_latest_backup_begin{{bank="{rbank}",vault="{rvault}",branch="{rbranch}"}} {begin}')
                print(f'dirvish_latest_backup_end{{bank="{rbank}",vault="{rvault}",branch="{rbranch}"}} {end}')
                print(f'dirvish_latest_backup_duration{{bank="{rbank}",vault="{rvault}",branch="{rbranch}"}} {duration}')
                print(f'dirvish_latest_backup_branch_length{{bank="{rbank}",vault="{rvault}",branch="{rbranch}"}} {len(images)}')
                if 'snapshot' in latest:
                    print(f'dirvish_latest_backup_snapshot_mtime{{bank="{rbank}",vault="{rvault}",branch="{rbranch}"}} {snapshot}')


def parse_args():
    parser = argparse.ArgumentParser(prog='prometheus-dirvish-collector')
    parser.set_defaults(func=lambda x: parser.print_help())
    parser.add_argument('-f',
                        metavar='master.conf',
                        type=str,
                        action='store',
                        dest='master_conf',
                        help='Path to the dirvish master config',
                        default='/etc/dirvish/master.conf')
    parser.add_argument('-s',
                        metavar='snapshot',
                        type=str,
                        action='store',
                        dest='snapshot_check_file',
                        help='Path to a file in the image tree of which to fetch the modification time',
                        default=None)
    return parser.parse_args(sys.argv[1:])


def main():
    args = parse_args()
    banks = parse_all_banks(args.master_conf, args.snapshot_check_file)
    generate_prometheus_response(banks)


if __name__ == '__main__':
    main()
