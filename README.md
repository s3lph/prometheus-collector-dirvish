# Prometheus Collector for Dirvish Backups

Expose your dirvish backup vault state as Prometheus metrics.

## Project Status

Don't expect regular updates.  If something breaks, I'll most likely
fix it tough, since I'm using this in production myself.


## Usage

Simply run this script, and point it to your dirvish `master.conf` file.

```sh
./prometheus-collector-dirvish.py -f /path/to/master.conf
```

The resulting metrics are written to stdout using Prometheus' plain
text exposition format.  There are two common ways to integrate this
into your Prometheus stack:

1.  Use the textfile collector feature of Prometheus' Node Exporter:
    Run this script in a cronjob and write the output to a file which
    is picked up by the Node Exporter, e.g.:
    
    ```sh
    /usr/local/bin/prometheus-collector-dirvish.py | sponge /var/lib/prometheus/node_exporter/dirvish.prom
    ```

2.  Push the metrics to a Prometheus Push Gateway: Again, run this
    script in a cronjob, and use `curl` etc. to submit the data to
    your Push Gateway:
    
    ```sh
    /usr/local/bin/prometheus-collector-dirvish.py | curl -d @- http://pushgateway:9091/metrics/job/dirvish/instance/backupserver
    ```


## Alerting Rules

Please see the file `dirvish_alert_rules.yml` for the set of alerting
rules I'm using in my setup.
